function SectionCreator() {
  let section = document.createElement("section");
  let title = document.createElement("p");
  let subtitle = document.createElement("p");
  let form = document.createElement("form");
  let input = document.createElement("input");
  let button = document.createElement("button");

  function create(type) {
    section.classList.add(
      "app-section__join-our-program",
      "app-section__image"
    );
    section.id = "joinoursection";
    title.classList.add("title");
    subtitle.classList.add("subtitle");
    form.classList.add("input-area");

    title.innerText =
      type === "standard" ? "Join Our Program" : "Join our advanced program";

    subtitle.innerHTML =
      "Sed do eiusmod tempor incididunt </br> ut labore et dolore magna aliqua.";
    form.appendChild(input);
    form.appendChild(button);
    input.id = "subscribe-value";
    button.id = "subscribe";
    button.innerText =
      type === "standard" ? "SUBSCRIBE" : "Subscribe to Advanced Program";

    input.setAttribute("type", "text");
    input.setAttribute("placeholder", "Email");
    button.setAttribute("type", "submit");
    section.appendChild(title);
    section.appendChild(subtitle);
    section.appendChild(form);

    let learnmore = document.getElementById("learn_more");
    learnmore.after(section);
  }
  function remove() {
    const section = document.getElementById("joinoursection");
    section.remove()
  }
  return {
    remove,
    create,
  }
}

export { SectionCreator };
