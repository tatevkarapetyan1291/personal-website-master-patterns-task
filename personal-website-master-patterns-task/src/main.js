import {SectionCreator} from './join-us-section.js';


document.addEventListener("DOMContentLoaded", function () {
  function run() {

    const JoinOurProgramSection = SectionCreator()
  JoinOurProgramSection.create('standard')

   document.getElementById("subscribe").addEventListener("click", (event) => {
    event.preventDefault();
    const val = document.getElementById("subscribe-value").value;
    console.log(val);
  });
  }

  if (document.readyState != "loading") run();
  else if (document.addEventListener)
    document.addEventListener("DOMContentLoaded", run);
  else
    document.attachEvent("onreadystatechange", function () {
      if (document.readyState == "complete") run();
    });
});
